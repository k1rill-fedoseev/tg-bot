#!/bin/bash

set -e

echo "Apply new migrations"
python manage.py collectstatic --noinput

echo "Apply new migrations"
python manage.py makemigrations
python manage.py migrate

echo "Run server"
python manage.py runserver --insecure 0:8000
