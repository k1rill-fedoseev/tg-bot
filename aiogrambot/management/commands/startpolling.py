from django.core.management.base import BaseCommand
from aiogram import executor

from ...apps import AiogramBot


class Command(BaseCommand):
    help = "Start telegram bot in polling mode"

    def handle(self, *args, **options):
        if AiogramBot.dispatcher is None:
            self.stdout.write("Bot is not initialized")
        else:
            executor.start_polling(AiogramBot.dispatcher, skip_updates=True)
