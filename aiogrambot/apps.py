import importlib

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from django.apps import AppConfig, apps
from django.conf import settings
from aiogram import Bot, Dispatcher

import logging

from django.utils.module_loading import module_has_submodule

logger = logging.getLogger(__name__)

BOT_MODULE_NAME = 'bot'


class AiogramBot(AppConfig):
    name = 'aiogrambot'
    verbose_name = 'Simple polling telegram bot app'
    ready_run = False

    bot = Bot(token=settings.TELEGRAM_BOT_TOKEN)
    dispatcher: Dispatcher = None

    def ready(self):
        if AiogramBot.ready_run:
            return
        AiogramBot.ready_run = True

        AiogramBot.dispatcher = Dispatcher(AiogramBot.bot, storage=MemoryStorage())

        def load_bot_module(module_name, method_name, execute):
            try:
                m = importlib.import_module(module_name)
                if execute and hasattr(m, method_name):
                    logger.debug(f'Run {module_name}.{method_name}()')
                    getattr(m, method_name)()
                else:
                    logger.debug(f'Run {module_name}')

            except ImportError as er:
                raise er

            return True

        for app_config in apps.get_app_configs():
            if module_has_submodule(app_config.module, BOT_MODULE_NAME):
                module_name = '%s.%s' % (app_config.name, BOT_MODULE_NAME)
                if load_bot_module(module_name, 'main', True):
                    logger.info('Loaded {}'.format(module_name))
