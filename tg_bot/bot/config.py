import logging
import os

from django.conf import settings

from aiogrambot.apps import AiogramBot
from ..models import Hotel


logger = logging.getLogger('bot')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

BOT_HOTEL_ID = settings.BOT_HOTEL_ID
HOTEL = {}
try:
    HOTEL = Hotel.objects.get(id=BOT_HOTEL_ID)
except Exception:
    pass

bot = AiogramBot.bot
dispatcher = AiogramBot.dispatcher
