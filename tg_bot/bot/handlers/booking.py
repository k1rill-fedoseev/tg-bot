from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
import aiogram.utils.markdown as md

from ..config import HOTEL, dispatcher, bot, BOT_HOTEL_ID
from ..keyboards import cancel_inline_keyboard, booking_menu_inline_keyboard, cancel_confirm_inline_keyboard
from ..fsm_states import BotStates
from ..messages import *
from ...models import Booking


async def send_booking_info(message, booking, state):
    context = await state.get_data()
    message_id = context.get('booking_menu_message_id', None)
    if message_id is not None:
        await bot.edit_message_reply_markup(chat_id=message.chat.id, message_id=message_id, reply_markup=None)
    response = await message.answer(
        md.text(
            md.text(md.bold('Booking ID:'), md.text(str(booking.id))),
            md.text(md.bold('Confirmation number:'), md.text(str(booking.confirmation_number))),
            md.text(md.bold('Hotel:'), md.text(HOTEL.name)),
            md.text(md.bold('Room:'), md.text(booking.room.name)),
            md.text(md.bold('Room category:'), md.text(booking.room.room_category.name)),
            md.text(md.bold('Dates:'), md.text(f"{booking.date_check_in} - {booking.date_check_out}")),
            md.text(md.bold('Status:'), md.text('active' if booking.active else 'cancelled')),
            sep='\n'
        ),
        reply_markup=booking_menu_inline_keyboard(booking.active),
        parse_mode=types.ParseMode.MARKDOWN
    )
    await state.update_data(booking_menu_message_id=response.message_id)
    await state.set_state(BotStates.view_booking_info)
    return response


# start simple booking conversation
@dispatcher.message_handler(Text(equals=CHECK_BOOKING_BUTTON_TEXT), state='*')
@dispatcher.message_handler(commands='my_booking', state='*')
async def booking_command(message: types.Message, state: FSMContext):
    context = await state.get_data()
    booking = context.get('booking', None)
    if booking is not None:
        # display booking menu
        await send_booking_info(message, booking, state)
        return
    await BotStates.expect_booking_id.set()

    # ask user for booking id
    response = await message.answer(ASK_FOR_BOOKING_ID, reply_markup=cancel_inline_keyboard)

    # remember this bot's message id
    await state.update_data(booking_message_id=response.message_id)


# user sends a booking id
@dispatcher.message_handler(state=BotStates.expect_booking_id)
async def receive_booking_id(message: types.Message, state: FSMContext):
    context = await state.get_data()

    # remove keyboard from previous bot message with id request
    await bot.edit_message_reply_markup(
        chat_id=message.chat.id,
        message_id=context['booking_message_id'],
        reply_markup=None
    )

    if not str.isdigit(message.text):
        await BotStates.start.set()
        await message.answer(INVALID_BOOKING_ID)
        return

    booking = None
    try:
        booking = Booking.objects.get(id=int(message.text), room__room_category__hotel_id=BOT_HOTEL_ID)
    except Booking.DoesNotExist:
        pass
    if booking is None:
        await BotStates.start.set()
        await message.answer(INVALID_BOOKING_ID)
        return

    await BotStates.expect_booking_confirmation.set()

    # ask user for confirmation code
    response = await message.answer(ASK_FOR_CONFIRMATION_CODE, reply_markup=cancel_inline_keyboard)

    # remember question message id
    await state.update_data(booking=booking, booking_message_id=response.message_id)


# user sends a confirmation code
@dispatcher.message_handler(state=BotStates.expect_booking_confirmation)
async def receive_confirmation_code(message: types.Message, state: FSMContext):
    context = await state.get_data()

    # remove keyboard from previous bot message with id request
    await bot.edit_message_reply_markup(
        chat_id=message.chat.id,
        message_id=context['booking_message_id'],
        reply_markup=None
    )

    booking = context['booking']

    if not str.isdigit(message.text) or booking.confirmation_number != int(message.text):
        await BotStates.start.set()
        await message.answer(INVALID_CONFIRMATION_CODE)
        await state.update_data(booking=None)
        return

    await BotStates.view_booking_info.set()

    # display booking menu
    await send_booking_info(message, booking, state)


# user cancels booking search
@dispatcher.callback_query_handler(
    lambda query: query.data == 'cancel',
    state=[BotStates.expect_booking_id, BotStates.expect_booking_confirmation]
)
async def cancel_booking_search(query: types.CallbackQuery, state: FSMContext):
    # reset state
    await BotStates.start.set()

    # update initial message
    await query.message.edit_reply_markup(None)
    await query.answer(CANCELLED_BOOKING_SEARCH)

    await state.update_data(booking=None)


# user asks to cancel his booking
@dispatcher.callback_query_handler(
    lambda query: query.data == 'discard',
    state=BotStates.view_booking_info
)
async def handle_booking_menu(query: types.CallbackQuery):
    # answer for confirmation
    await query.answer()
    await query.message.answer(CONFIRM_BOOKING_CANCELLATION, reply_markup=cancel_confirm_inline_keyboard)

    # reset state
    await BotStates.request_booking_cancellation.set()


# confirm booking cancellation
@dispatcher.callback_query_handler(
    lambda query: query.data == 'confirm',
    state=BotStates.request_booking_cancellation
)
async def confirm_booking_cancellation(query: types.CallbackQuery, state: FSMContext):
    context = await state.get_data()
    booking = context['booking']
    booking_menu_message_id = context['booking_menu_message_id']

    booking.active = False
    booking.save()
    await query.message.edit_reply_markup(None)
    await bot.delete_message(
        chat_id=query.message.chat.id,
        message_id=booking_menu_message_id
    )
    await state.update_data(booking_menu_message_id=None, booking=booking)
    await send_booking_info(query.message, booking, state)
    await query.message.delete()
    await query.answer(BOOKING_WAS_CANCELLED)
    # reset state
    await BotStates.start.set()


# reject booking cancellation
@dispatcher.callback_query_handler(
    lambda query: query.data == 'cancel',
    state=BotStates.request_booking_cancellation
)
async def reject_booking_cancellation(query: types.CallbackQuery):
    await query.answer()
    await query.message.delete()
    await BotStates.view_booking_info.set()


# ask for support
@dispatcher.callback_query_handler(
    lambda query: query.data == 'support',
    state=BotStates.view_booking_info
)
async def support_command(query: types.CallbackQuery, state: FSMContext):
    await BotStates.expect_support_question.set()
    await query.answer()

    # ask user for question
    response = await query.message.answer(ASK_FOR_QUESTION, reply_markup=cancel_inline_keyboard)

    # remember this bot's message id
    await state.update_data(support_type='booking', support_message_id=response.message_id)


# start booking support conversation
@dispatcher.callback_query_handler(
    lambda query: query.data == 'reset_booking',
    state=BotStates.view_booking_info
)
async def support_command(query: types.CallbackQuery, state: FSMContext):
    await BotStates.expect_booking_id.set()
    await query.answer()
    await state.update_data(booking_menu_message_id=None, booking=None)

    # remove keyboard
    await query.message.edit_reply_markup(None)
