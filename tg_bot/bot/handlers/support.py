from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
import aiogram.utils.markdown as md

from ..config import HOTEL, dispatcher, bot
from ..keyboards import cancel_inline_keyboard, cancel_submit_inline_keyboard
from ..fsm_states import BotStates
from ..messages import *


# start simple support conversation
@dispatcher.message_handler(Text(equals=SUPPORT_BUTTON_TEXT), state='*')
@dispatcher.message_handler(commands='support', state='*')
async def support_command(message: types.Message, state: FSMContext):
    await BotStates.expect_support_question.set()

    # ask user for question
    response = await message.answer(ASK_FOR_QUESTION, reply_markup=cancel_inline_keyboard)

    # remember this bot's message id
    await state.update_data(support_type='simple', support_message_id=response.message_id)


# user sends a question
@dispatcher.message_handler(state=BotStates.expect_support_question)
async def receive_question(message: types.Message, state: FSMContext):
    await BotStates.confirm_support_question.set()
    context = await state.get_data()

    # remove keyboard from previous bot message with asking for question
    await bot.edit_message_reply_markup(
        chat_id=message.chat.id,
        message_id=context['support_message_id'],
        reply_markup=None
    )

    # ask user for confirmation
    await message.answer(CONFIRM_QUESTION, reply_markup=cancel_submit_inline_keyboard)

    # remember question message id
    await state.update_data(support_message_id=message.message_id)


# user presses cancel
@dispatcher.callback_query_handler(
    lambda query: query.data == 'cancel',
    state=[BotStates.expect_support_question, BotStates.confirm_support_question]
)
async def cancel_received_question(query: types.CallbackQuery, state: FSMContext):
    context = await state.get_data()

    # reset state
    if context.get('support_type', 'simple') != 'simple':
        await BotStates.view_booking_info.set()
    else:
        await BotStates.start.set()

    # update initial message
    await query.message.edit_text(CANCEL_SUPPORT_REQUEST, reply_markup=None)


# user presses submit
@dispatcher.callback_query_handler(
    lambda query: query.data == 'submit',
    state=BotStates.confirm_support_question
)
async def submit_received_question(query: types.CallbackQuery, state: FSMContext):
    context = await state.get_data()

    simple = context.get('support_type', 'simple') == 'simple'

    #reset state
    if not simple:
        await BotStates.view_booking_info.set()
    else:
        await BotStates.start.set()

    # send notification to admin
    await bot.send_message(
        HOTEL.admin_telegram_id,
        NEW_SIMPLE_REQUEST_NOTIFICATION
        if simple is True else
        BOOKING_SUPPORT_REQUEST_NOTIFICATION(
            context['booking'].id
        )
    )
    # forward message to admin
    await bot.forward_message(
        chat_id=HOTEL.admin_telegram_id,
        from_chat_id=query.message.chat.id,
        message_id=context['support_message_id']
    )

    # update initial message
    await query.message.edit_text(ACCEPTED_SUPPORT_REQUEST, reply_markup=None)


# admin replies to forwarded message
@dispatcher.message_handler(lambda message: message.chat.id == HOTEL.admin_telegram_id)
async def handle_admin_reply(message: types.Message):
    # initial chat id
    chat_id = message.reply_to_message.forward_from.id
    # send user a response from admin
    await bot.send_message(
        chat_id=chat_id,
        text=md.text(
            md.text(md.bold('Question:'), md.text(message.reply_to_message.text)),
            md.text(md.bold('Response:'), md.text(message.text)),
            sep='\n'
        ),
        parse_mode=types.ParseMode.MARKDOWN
    )
