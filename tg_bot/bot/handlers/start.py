from aiogram import types
from aiogram.dispatcher.filters import Text

from ..config import BOT_HOTEL_ID, HOTEL, dispatcher
from ..keyboards import start_keyboard, hotel_info_keyboard
from ..fsm_states import BotStates
from ..messages import *
from ...models import HotelInfo


@dispatcher.message_handler(commands=['start', 'help'], state='*')
async def start(message: types.Message):
    await BotStates.start.set()
    await message.answer(START_MESSAGE, reply_markup=start_keyboard)


@dispatcher.message_handler(Text(equals=HOTEL_INFO_BUTTON_TEXT), state='*')
@dispatcher.message_handler(commands='hotel_info', state='*')
async def hotel_info(message: types.Message):
    await BotStates.hotel_info.set()
    infos = HotelInfo.objects.filter(hotel=BOT_HOTEL_ID)
    keyboard = hotel_info_keyboard(infos, HOTEL.website)
    await message.answer(HOTEL_INFO_STUB, reply_markup=keyboard)
