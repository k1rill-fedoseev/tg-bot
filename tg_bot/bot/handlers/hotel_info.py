from aiogram import types

from ..config import dispatcher
from ..fsm_states import BotStates
from ...models import HotelInfo


@dispatcher.callback_query_handler(state=BotStates.hotel_info)
async def update_hotel_info(query: types.CallbackQuery):
    info_id = int(query.data)
    info = HotelInfo.objects.get(id=info_id)
    if info.content != query.message.text:
        await query.message.edit_text(text=info.content, reply_markup=query.message.reply_markup)
    await query.answer()
