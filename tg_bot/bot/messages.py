START_MESSAGE = '''
Hi, I am a simple hotel management bot!
Here are my command:
/start or /help to see this message
'''

HOTEL_INFO_STUB = '''
Use below buttons to see the available information
'''

OUR_WEBSITE = '''
Go to our website
'''

ASK_FOR_QUESTION = '''
Please, send in the next message your question:
'''

CANCEL_SUPPORT_REQUEST = '''
Cancelled support request
'''

CONFIRM_QUESTION = '''
Are you sure, you want to send this message
'''

ACCEPTED_SUPPORT_REQUEST = '''
Accepted your request, an administrator will reply to you in a minute.
'''

NEW_SIMPLE_REQUEST_NOTIFICATION = '''
New support request arrived:
'''

OUTDATED_BOOKING_INFO = '''
Outdated booking info
'''

BOOKING_WAS_CANCELLED = '''
Booking was cancelled
'''

CONFIRM_BOOKING_CANCELLATION = '''
Are you sure you want to cancel your booking
'''

CANCELLED_BOOKING_SEARCH = '''
Cancelled booking search
'''

INVALID_CONFIRMATION_CODE = '''
Confirmation code is invalid
'''

INVALID_BOOKING_ID = '''
Booking id is invalid
'''

ASK_FOR_BOOKING_ID = '''
Please, enter your booking id
'''

ASK_FOR_CONFIRMATION_CODE = '''
Please, enter a confirmation code
'''


def BOOKING_SUPPORT_REQUEST_NOTIFICATION(booking_id):
    return f'''
New booking support request arrived, booking id {booking_id}:
'''


HOTEL_INFO_BUTTON_TEXT = 'Hotel Info'
SUPPORT_BUTTON_TEXT = 'Contact administrator'
CHECK_BOOKING_BUTTON_TEXT = 'Check my booking'
CANCEL_BUTTON_TEXT = 'Cancel'
SUBMIT_BUTTON_TEXT = 'Submit'
CANCEL_BOOKING_BUTTON_TEXT = 'Cancel this booking'
CONFIRM_BUTTON_TEXT = 'Confirm'
RESET_BOOKING_BUTTON_TEXT = 'Choose another one'
