from aiogram import types

from .messages import *

start_keyboard = types.ReplyKeyboardMarkup()
start_keyboard.insert(types.KeyboardButton(HOTEL_INFO_BUTTON_TEXT))
start_keyboard.insert(types.KeyboardButton(SUPPORT_BUTTON_TEXT))
start_keyboard.insert(types.KeyboardButton(CHECK_BOOKING_BUTTON_TEXT))

cancel_inline_keyboard = types.InlineKeyboardMarkup()
cancel_inline_keyboard.insert(types.InlineKeyboardButton(CANCEL_BUTTON_TEXT, callback_data='cancel'))

cancel_submit_inline_keyboard = types.InlineKeyboardMarkup()
cancel_submit_inline_keyboard.insert(types.InlineKeyboardButton(CANCEL_BUTTON_TEXT, callback_data='cancel'))
cancel_submit_inline_keyboard.insert(types.InlineKeyboardButton(SUBMIT_BUTTON_TEXT, callback_data='submit'))

cancel_confirm_inline_keyboard = types.InlineKeyboardMarkup()
cancel_confirm_inline_keyboard.insert(types.InlineKeyboardButton(CANCEL_BUTTON_TEXT, callback_data='cancel'))
cancel_confirm_inline_keyboard.insert(types.InlineKeyboardButton(CONFIRM_BUTTON_TEXT, callback_data='confirm'))


def hotel_info_keyboard(infos, website):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.insert(types.InlineKeyboardButton(OUR_WEBSITE, url=website))
    keyboard.row()
    for info in infos:
        keyboard.insert(
            types.InlineKeyboardButton(
                info.button_name,
                callback_data=str(info.id)
            )
        )
    return keyboard


def booking_menu_inline_keyboard(active):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(RESET_BOOKING_BUTTON_TEXT, callback_data='reset_booking'))
    if active:
        keyboard.add(types.InlineKeyboardButton(CANCEL_BOOKING_BUTTON_TEXT, callback_data='discard'))
    keyboard.add(types.InlineKeyboardButton(SUPPORT_BUTTON_TEXT, callback_data='support'))
    return keyboard
