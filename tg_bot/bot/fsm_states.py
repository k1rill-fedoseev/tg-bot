from aiogram.dispatcher.filters.state import StatesGroup, State


class BotStates(StatesGroup):
    start = State()

    hotel_info = State()

    expect_support_question = State()
    confirm_support_question = State()

    expect_booking_id = State()
    expect_booking_confirmation = State()
    view_booking_info = State()
    request_booking_cancellation = State()
