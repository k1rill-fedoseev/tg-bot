from django.http import HttpResponse
from django.template import loader

from .models import Hotel
from .settings import BOT_HOTEL_ID


def index(request):
    hotel = Hotel.objects.get(id=BOT_HOTEL_ID)
    template = loader.get_template('index.html')
    context = {
        'hotel': hotel
    }
    return HttpResponse(template.render(context, request))
