from django.db import models

from . import Hotel


class HotelInfo(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    button_name = models.CharField(max_length=30)
    content = models.CharField(max_length=1000)

    def __str__(self):
        return f'{self.hotel} - {self.button_name}'
