from django.db import models


class Hotel(models.Model):
    name = models.CharField(max_length=50)
    admin_telegram_id = models.IntegerField()
    website = models.CharField(max_length=256)

    def __str__(self):
        return self.name
