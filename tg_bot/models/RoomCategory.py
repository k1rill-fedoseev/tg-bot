from django.db import models

from . import Hotel


class RoomCategory(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    min_price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return f'{self.name} at {self.hotel}'
