from django.db import models

from . import Room


class Booking(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    confirmation_number = models.IntegerField()
    date_check_in = models.DateField()
    date_check_out = models.DateField()
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'Booking {self.room} from {self.date_check_in} to {self.date_check_out}'
