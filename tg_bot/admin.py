from django.contrib import admin

from .models import User, RoomCategory, Room, Hotel, HotelInfo, Booking

admin.site.register(User)
admin.site.register(Hotel)
admin.site.register(HotelInfo)
admin.site.register(RoomCategory)
admin.site.register(Room)
admin.site.register(Booking)
