# Telegram bot for hotel management system
Test internship challenge. Combines both TelegramBot and DevOps problems.

## Stack
* Django
* Aiogram
* Docker
* Nginx
* GitLab CI/CD
* SQLite3

## Bot Features
All requested features were implemented in some manner.
* User can see hotel info, info is retrieved from database
* User can see his booking by entering booking id and confirmation code
* User can ask for general support
* User can ask for booking specific support
* Bot forwards support requests to admin
* Admin can answer on user support request by replying to forwarded messages in the bot
* User can cancel his booking

## Bot implementation details
Bot uses some simple FSM under the hood, provided by aiogram library.
Confirmation dialogs with inline keyboards are used
for action confirmation, such as booking cancellation or sending support request.

## CD details
Separate instance is used for production deployment on AWS.
GitLab CI/CD environment accesses the remote docker 
engine by setting `DOCKER_HOST` environment variable. 
Generated SSH keys are used for accessing remote machine.
Docker-compose rebuilds and restarts the application for 
each commit in the master branch. 

A DNS name `fedoseev.live` was occupied from [www.name.com](www.name.com).
Generated SSL certificates are saved on the remote machine, docker mounts them into nginx container.

## To run on localhost
Create `.env` file with `BOT_TOKEN`, see `.env.example`. 
```shell script
docker-compose up --build -d
docker-compose exec -d django_app python manage.py startpolling
```
Go on `localhost:8000` or communicate with your bot.

For accessing admin panel, create second superuser:
```shell script
docker-compose exec django_app python manage.py createsuperuser
```
